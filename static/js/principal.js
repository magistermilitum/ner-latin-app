//carga de los botones en hidde/display-->
$( document ).ready(function() {
    var divs = ["Div1", "Div2", "Div3", "Div4", "Div5", "Div6"];
        var visibleDivId = null;
        function divVisibility(divId) {
          if(visibleDivId === divId) {
            visibleDivId = null;
          } else {
            visibleDivId = divId;
          }
          hideNonVisibleDivs();
        }
        function hideNonVisibleDivs() {
          var i, divId, div;
          for(i = 0; i < divs.length; i++) {
            divId = divs[i];
            div = document.getElementById(divId);
            if(visibleDivId === divId) {
              div.style.display = "block";
            } else {
              div.style.display = "none";
            }
          }
        }
    
    
    
    //codemirror instancia para xml tei-->
    
        document.getElementById("test").value = vkbeautify.xml(document.getElementById("test").value);
        CodeMirror.fromTextArea(document.getElementById("test"), {
          mode: 'application/xml',
          // theme: 'eclipse',
          lineNumbers: true,
          lineWrapping: true,
          readOnly: false,
          cursorBlinkRate: 0,
          autoRefresh:true,
        });    
      
      
    
    
      var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
            lineNumbers: true,
            mode: "application/ld+json",
            lineWrapping: true,
          readOnly: false,
          cursorBlinkRate: 0,
            autoRefresh:true,
          });
    
            
    
    
    
      var editor = CodeMirror.fromTextArea(document.getElementById("code_2"), {
            lineNumbers: true,
            mode: "application/ld+json",
            lineWrapping: true,
          readOnly: false,
          cursorBlinkRate: 0,
            autoRefresh:true,
          });
    
            
    
    
    //script para la pagina de pre-cargado-->  
    
    function onReady(callback) {
        var intervalID = window.setInterval(checkReady, 1000);
    
        function checkReady() {
            if (document.getElementsByTagName('body')[0] !== undefined) {
                window.clearInterval(intervalID);
                callback.call(this);
            }
        }
    }
    
    function show(id, value) {
        document.getElementById(id).style.display = value ? 'block' : 'none';
    }
    
    onReady(function () {
        show('loading', false);
    });  
      
    
    //script para contar palabras en textarea con limite-->
    
       $(document).ready(function() {
    var text_max = 10000;
    $('#textarea_feedback').html(text_max + ' /10000');
    
    $('#textarea_1').keyup(function() {
        var text_length = $('#textarea_1').val().length;
        var wordCount = $('#textarea_1').val().replace( /[^\w ]/g, "" ).split( /\s+/ ).length
        var text_remaining = text_max - wordCount;
        
    
        $('#textarea_feedback').html(text_remaining + ' /10000');
        
        if (wordCount > 8500) {
        $('#textarea_feedback').css('color', '#F5102F');
      }else {
        $('#textarea_feedback').css('color','#719ECE');
      };
      
    });
    
    
    });
        
        
    //permitir cargar un archivo de texto personal-->	
    
    $('#customFile').on('change',function(){
                    //get the file name
                    var fileName = $(this).val().replace('C:\\fakepath\\', " ");
                    //replace the "Choose a file" label
                    $(this).next('.custom-file-label').html(fileName);
                })
    
    
    //permite descargar un fichero a voluntad -->
    
    function download(text, name, type)
        {
            var file = new Blob([text], {type: type});
            var isIE = /*@cc_on!@*/false || !!document.documentMode;
            if (isIE)
            {
                window.navigator.msSaveOrOpenBlob(file, name);
            }
            else
            {
                var a = document.createElement('a');
                a.href = URL.createObjectURL(file);
                a.download = name;
                a.click();
            }
         }
      
    
    
    //instanciacion de label studio-->
    
    
      function decodeJSON(encodedJSON) {
      var decodedJSON = $('<div/>').html(encodedJSON).text();
      return $.parseJSON(decodedJSON);
     }
    
     var conejo=decodeJSON('{{dict_json}}');
     var conejo_2=conejo
     for(i = 0; i <= Object.keys(conejo).length; i++) {
      var numero='swap_'+i
      var conjunto=conejo[numero]
    
    
      
      var labelStudio = new LabelStudio(numero, {
        config: `
          <View>
          <Labels name="label" toName="text">
            <Label value="Pers" background="#5C4FB2"/>
            <Label value="Organization" background="darkorange"/>
            <Label value="Loc" background="#2F8389"/>
          </Labels>
    
          <Text name="text" value="$text"/>
        </View>
        `,
    
        interfaces: [ "update","controls", "submit", "side-column","completions:menu", "completions:add-new","completions:delete" ],
        
        messages : {
          DONE: "Done!",
        }
    ,
        task: conjunto,
        
        onLabelStudioLoad: function(LS) {
          var c = LS.completionStore.addCompletion({
            userGenerate: true
          });
          LS.completionStore.selectCompletion(c.id);
        },
      onSubmitCompletion: function(LS, C) {
              console.log(JSON.stringify(C.serializeCompletion()));
             
              alert("Submit button pressed");
          },
          onUpdateCompletion: function (LS, C) {
            console.log(JSON.stringify(C.serializeCompletion()));
    
            var numeral='swap_'+Object.values(C.serializeCompletion()[0].value.text.split("_")[1]);
            conejo_2[numeral]=C.serializeCompletion();
            
    
            var r = confirm("File succesfully updated\n Do you want to download new annotation?");
            if (r == true) {
              download(JSON.stringify(conejo_2, null, 3), 'Form_Data_.json','application/json');
            } 
        },
        
    
      });
    
      
    }
    
    
    
    
    
    //en este script transformamos json a un formato de columnas como el usado por brat
    //debe estar aqui para que la transformacion se haga al presionar el boton de descarga, que se halla al final del script
    
    var conejo_3=conejo
    var boite = {}
    i=0;
          
          for (var key in conejo_3) {
              var texto=conejo_3[key].data.text;
            boite[key] = i;
          //boite.push(key)
    
    
          i+=texto.length+1;}
    
    
    
    var csv = arr_2.map(function(d){
        return d.join("\t");
    }).join('\n');
           
    
    
    
    
    
      
      function exportJson(el) {
        
        var arr_2 = [];
        var orden=0;
        var conejo_4=conejo_2
          
          for (var key_2 in conejo_4) {
                for(i = 0; i < conejo_4[key_2].length; i++){
                  var arr_3=[key_2]
                  var datos=conejo_4[key_2][i].value
                  for (var key_3 in datos){
                      arr_3.push(datos[key_3])
              }
              arr_3[1]=arr_3[1]+boite[arr_3[0]]
              arr_3[2]=arr_3[2]+boite[arr_3[0]]
              arr_3[3]=arr_3[3].split("_")[0]
              arr_3[4]=arr_3[4][0]
              //ordenes para modificar el orden de la lista principal
              orden+=1;
              arr_3.splice(0,1);
              arr_3.splice(2, 0, arr_3[3]);
              arr_3.splice(4);
              arr_3.splice(0, 0, "T"+orden);
              arr_2.push(arr_3);    
            }
    };
    
    //ordenar lista por primer elemento
    arr_2.sort(function(a,b){return a[0] - b[0];}); 
    
    //transformar lista a tsv
    var csv = arr_2.map(function(d){
        return d.join("\t");
    }).join('\n');
    
    //var tsv = arr_2.join("\t");
           
      //var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(csv, null, 3));
      //var data = "text/csv;charset=utf-8," + encodeURIComponent(csv);
      var data= "data:text/tab-separated-values," + encodeURIComponent(csv);
    // what to return in order to show download window?
    
    el.setAttribute("href", "data:"+data);
    //el.setAttribute("download", "data.json");
    //el.setAttribute("download", "data.csv");
    el.setAttribute("download", "data.txt");
    
    
    }
      
    
    
      $(function(){
        //this will hide all elements with the class 'portswap';
        //Recommend you set the style (CSS) of PortSwap to 'display: none',
        //so as to avoid flash of unstyled content;
        $(".PortSwap").not('#swap_0').hide();
        $('#swap_0').addClass("active");
        $(".PortSwap_2").not('#swap_0swap_0').hide();
        $('#swap_0swap_0').addClass("active");
        //<input type="button" id="{{ key }}{{ key }}" alt="Previous" class="PortSwap_2" value="{{ key }}"/>
       
        //swap_0swap_0
    
        $('.btn_nav').on('click',function(){
            var activediv = $('.PortSwap.active').first();
            var activediv_id = parseInt(activediv[0].id.split('_')[1]);
            var activediv_2 = $('.PortSwap_2.active').first();
            var activediv_id_2 = parseInt(activediv_2[0].id.split('_')[2]);
            
            
            switch(this.id){
                //execute code when backward button is pressed
                case 'btn_backward':
                    if(activediv_id-1 >= 0){
                        //previous iteration of div is present, show it
                        activediv.removeClass('active').hide();  
                        activediv_2.removeClass('active').hide();                                
                        $('#swap_'+(activediv_id-1)).show().addClass('active');
                        $('#swap_'+(activediv_id_2-1)+'swap_'+(activediv_id_2-1)).show().addClass('active');
                    } else {
                        //No previous div is present, remain on same page.
                    }
                    break;
                //execute code when forward button is pressed
                case 'btn_forward':
                    if(activediv_id+1 < $('.PortSwap').length){
                        //Next iteration of div is present, show it
                        activediv.removeClass('active').hide();
                        activediv_2.removeClass('active').hide();                                
                        $('#swap_'+(activediv_id+1)).show().addClass('active');
                        $('#swap_'+(activediv_id_2+1)+'swap_'+(activediv_id_2+1)).show().addClass('active');
                    } else {
                        //No next div is present, remain on same page.
                    }                            
                    break;
            }
        }); 
    });
    
    
    
    
    
    var coolbutton = document.getElementById('exportJSON');
        var inprogress = false;
        coolbutton.onclick = function(){
        exportJson(this);
            if (inprogress) {
                return false;
            }
            inprogress = true
            coolbutton.classList.add('coolass_button_first');
            setTimeout(function(){
            coolbutton.classList.add('coolass_button_bridge1');
            },500);
            setTimeout(function(){
            coolbutton.classList.add('coolass_button_second');
            },600);
            setTimeout(function(){
            coolbutton.classList.add('coolass_button_third');
            },700);
            setTimeout(function(){
            coolbutton.classList.add('coolass_button_final');
            },1800);
            setTimeout(function(){
                coolbutton.classList.remove('coolass_button_final');
                coolbutton.classList.remove('coolass_button_third');
                coolbutton.classList.remove('coolass_button_second');
                coolbutton.classList.remove('coolass_button_bridge1');
                coolbutton.classList.remove('coolass_button_first');
                inprogress = false;
            },3200)
        };
    
    
    
    
    
    $(function() {
    
    var i = 0;
    
    $("colgroup").each(function() {
        i++;
        $(this).attr("id", "col"+i);
    
    });
    
    var totalCols = i;
    i = 1;
    $("td").each(function() {
        $(this).attr("rel", "col"+i);
        i++;
        if (i > totalCols) { i = 1; }
    });
    
    $("td").hover(function() {
    
        $(this).parent().addClass("hover_1");
        var curCol = $(this).attr("rel");
        $("#"+curCol).addClass("hover_1");
    
    }, function() {
    
        $(this).parent().removeClass("hover_1");
        var curCol = $(this).attr("rel");
        $("#"+curCol).removeClass("hover_1");
    
    });
    
    });
    
    
    
    
    
    
    
      var svg = d3.select("svg"),
          width = +svg.attr("width"),
          height = +svg.attr("height"),
          g = svg.append("g").attr("transform", "translate(" + (width / 2 + 40) + ",400)");
      
      var tree = d3.tree()
          .size([2 * Math.PI, 300])
          .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });
      
      d3.json("data.json", function(error, data) {
        if (error) throw error;
      
        var root = tree(d3.hierarchy(data));
      
        var link = g.selectAll(".link")
          .data(root.links())
          .enter().append("path")
            .attr("class", "link")
            .attr("d", d3.linkRadial()
                .angle(function(d) { return d.x; })
                .radius(function(d) { return d.y; }));
      
        var node = g.selectAll(".node")
          .data(root.descendants())
          .enter().append("g")
            .attr("class", function(d) { return "node" + (d.children ? " node--internal" : " node--leaf"); })
            .attr("transform", function(d) { return "translate(" + radialPoint(d.x, d.y) + ")"; });
      
        node.append("circle")
            .attr("r", 2.5);
      
        node.append("text")
            .attr("dy", "0.31em")
            .attr("x", function(d) { return d.x < Math.PI === !d.children ? 6 : -6; })
            .attr("text-anchor", function(d) { return d.x < Math.PI === !d.children ? "start" : "end"; })
            .attr("transform", function(d) { return "rotate(" + (d.x < Math.PI ? d.x - Math.PI / 2 : d.x + Math.PI / 2) * 180 / Math.PI + ")"; })
            .text(function(d) {
                return d.data.id
            });
      });
      
      function radialPoint(x, y) {
        return [(y = +y) * Math.cos(x -= Math.PI / 2), y * Math.sin(x)];
      }
      
    });
      