import numpy as np
from flask import Flask, request, jsonify, render_template, url_for, Markup, make_response, Response, json
import pickle
#from werkzeug import secure_filename

from joblib import dump, load
import pandas as pd
import treetaggerwrapper
import os
import sys
import subprocess

from flair.models import SequenceTagger
from flair.data import Sentence

from collections import Counter

from fuzzywuzzy import fuzz




app = Flask(__name__)

PERS_LOC_model  = SequenceTagger.load('models/best-model_all_b-loc_14_03_2022.pt') #PERS_LOCATIONS models

classes="B-PERS", "I-PERS"
classes_2="B-LOC", "I-LOC"

#Path to TreeTagger and avoid admin rights
current_path=os.path.abspath(os.getcwd())
subprocess.check_call(['chmod', '+x', current_path+"/treetagger/bin/tree-tagger"])


#transforming entry words to json dictionaries


inicio="<standOff><listEvent><event><label>First NER document</label><desc>"
final="</desc></event></listEvent></standOff>"

#reemplacing v and j to adapt text to lemmatizer

forggiven_characters=[("ę", "e"), ("æ", "e"), ("œ", "e"), ("<", ""), ("(", ""), (")", ""), ("\xa0", ""),
                     (">", ""), ("*", ""), ("[", ""), ("]", ""), ("«", ""), ("»", ""), 
                     (",", " ,"), (".", " ."), (";", " ;"), ("  ", " "), (" | | ", " "), (" | |", ""), ("\n", ""), ("\t", ""),
                     ("V", "U"), ("v", "u"), ("J", "I"), ("j", "i")]

def grafias(x):
    for grafia in forggiven_characters:
        x=str(x).replace(grafia[0], grafia[1])
        #x=x.lower()
        #x=x.split()
    return x

def grafias_b(x):
  x=str(x).replace("ñ", "'")
  return x

# The Omnia lemmatizer used and old unicode package which is not well recognized by many modern application. In order to correct that, an utf-conversion must be applied
def transcription(x):
  try:
    lemma=x.split("_", 1)[0]
    tr=x.split("_", 1)[1]
    try:
      tr=bytes(tr, encoding="raw_unicode_escape").decode("utf-8")
      return lemma, tr
    except:
      return lemma, tr
  except:
    return bytes(x, encoding="raw_unicode_escape").decode("utf-8"), "-"
		

#decoding to latin-1
def traduction(x):
	x=str(x)
	x=x.encode("latin-1").decode("latin-1") #el resultado estaba en latin-1, no reconocible por el html
	if len(x.split())>1:
		x=x.split()[1]
	else:
		x=x
	return x
		
#retorna el index de las entidades nombradas en la frase a partir de la columna BIO
# la clave esta en generar una lista del tipo:
# ['B-PERS', 80, 81],['B-LOC', 12, 13],['B-LOC', 22, 23],
# de modo que podamos recuperar pers, locs o ambos		
def indexation(clase, row, m):
	app=[x[row] for x in m]
	txt=[x[0] for x in m]
	caja=[]
	for index, item in enumerate(app):
		if item=="O":
			caja.append(("a", index))
		elif item==clase:
				caja.append(("b", index))
	indices=[]
	
	for index, x in enumerate(caja):
		if index<len(caja)-1:
			if x[0]=="b":
				a=len(" ".join(txt[:x[1]]))#indices por caracter
				b=len(" ".join(txt[x[1]:caja[index+1][1]]))
				indices.append([clase, x[1], caja[index+1][1], [a, a + b ]])
				
	return indices

#generación de las etiquetas segun el tipo de contenido
# etiquetas tei generan balisas person, si no, generan balisas html	
def tei_tags(tipo, entity_class):
	if tipo=="tei":
		if entity_class=="B-PERS":
			tag_1='<name type="person" subtype="complex">'
			tag_2='<name type="person" subtype="simple">'
			tag_3=' </name>'
		elif entity_class=="B-LOC":
			tag_1='<name type="place" subtype="complex">'
			tag_2='<name type="place" subtype="simple">'
			tag_3=' </name>'
	else:
		if entity_class=="B-PERS":
			tag_1='<strong class="double"><span style="border:1px; border-style:solid; border-radius: 5px; background: #E7E4FA; border-color:#7D73C1;padding: 3px;">'
			tag_2='<strong class="simple"><span style="border:1px; border-style:solid; border-radius: 5px; background: #E7E4FA; border-color:#7D73C1;padding: 3px;">'
			tag_3='</span></strong>'
		elif entity_class=="B-LOC":
			tag_1='<strong class="double"><span style="border:1px; border-style:solid; border-radius: 5px; background: #BBEDF0; border-color:#5F9FA4; padding: 1px;">'
			tag_2='<strong class="simple"><span style="border:1px; border-style:solid; border-radius: 5px; background: #BBEDF0; border-color:#5F9FA4 ; padding: 1px;">'
			tag_3='</span></strong>'
			
	return tag_1, tag_2, tag_3
		
	
	
#a partir del index generamos las etiquetas segun la talla de la entidad, indice[0] indica el tipo de entidad
# indices[1] y [2] el inicio y fin. Vamos modificando progrsivamente el texto añadiendo las balisas a las palabras.	
#<name type="person" subtype="complex">
def tei_build(posiciones, formato):
	texto=[x[0] for x in BIO_CONLL]
	for indice in posiciones:
		tag_1, tag_2, tag_3 = tei_tags(formato, indice[0])
		
		#a='<persName type="complex"> '+" ".join(entidad)+" </persName>"
		#tag_1="< entidad "+indice[0]+" >"
		#tag_2="</ "+"entidad"+" >"
		
			
		if int(indice[2])-int(indice[1])==2:
			inicio=tag_1+texto[int(indice[1])]
			final=texto[int(indice[2])-1]+tag_3
			nueva=[inicio]+[final]
			texto=texto[:int(indice[1])]+nueva+texto[int(indice[2]):]

		elif int(indice[2])-int(indice[1])>2 :
			inicio=tag_1+texto[int(indice[1])]
			medio=[x for x in texto[int(indice[1])+1:int(indice[2])-1]]
			final=texto[int(indice[2])-1]+tag_3
			nueva=[inicio]+medio+[final]
			texto=texto[:int(indice[1])]+nueva+texto[int(indice[2]):]

		else:
			nueva=[tag_2+texto[int(indice[1])]+tag_3]
			texto=texto[:int(indice[1])]+nueva+texto[int(indice[2]):]
			
	return texto


#generacion del json para label-studio

def label_studio(index, m, ID=1):
	texto=" ".join([x[0] for x in m])	
	big_dict=[]
	keys=["start", "end", "text", "labels", "marca"]
	for n, x in enumerate(index):
		if n==0 and index[n][3][0]==0:
			values=[x[3][0], x[3][1], texto[x[3][0]:x[3][1]+1]+"_"+str(ID), ["Loc"], ID ]
			values_b=[x[3][0], x[3][1], texto[x[3][0]:x[3][1]+1]+"_"+str(ID), ["Pers"], ID ]
		else:
			values=[x[3][0]+1, x[3][1]+1, texto[x[3][0]:x[3][1]+1]+"_"+str(ID), ["Loc"], ID ]
			values_b=[x[3][0]+1, x[3][1]+1, texto[x[3][0]:x[3][1]+1]+"_"+str(ID), ["Pers"], ID ]

		if x[0]=="B-LOC":
			dict_2={k: v for k, v in zip(keys, values)}
		else:
			dict_2={k: v for k, v in zip(keys, values_b)}
			
		keys_1=["id", "from_name", "to_name", "source", "type", "value"]
		values_1=[n, "label", "text", "$text", "labels", dict_2, ID]
		
		dict_3={k: v for k, v in zip(keys_1, values_1)}
		big_dict.append(dict_3)

	keys_2=["lead_time", "result", "id"]
	values_2=[ID, big_dict, ID]
	dict_4={k: v for k, v in zip(keys_2, values_2)}

	textualis={"text": texto}
	
	keys_3=["completions", "data"]
	values_3=[[dict_4], textualis]
	dict_5={k: v for k, v in zip(keys_3, values_3)}
	return dict_5


def centroid_forms(conjunto, n):
	entity_forms_groups={}
	texto=[x[0] for x in BIO_CONLL]
	entity_forms_groups["PERSONS"]=[" ".join(n[x[1]:x[2]]) for x in conjunto if "PERS" in x[0]]
	entity_forms_groups["LOCATIONS"]=[" ".join(n[x[1]:x[2]]) for x in conjunto if "LOC" in x[0]]

	forms_index=[[" ".join(n[x[1]:x[2]]), [x[1], x[2]]] for x in conjunto] #each entity and his index

	for k,v in entity_forms_groups.items():
		grs = list() # groups of names with distance > 80
		for name in v:
			for g in grs:
				if len(name.split())>0:
					if all(len(w.split())>0 for w in g) and all(fuzz.token_sort_ratio(name, w) > 72 for w in g) and all(name.startswith(w[0]) for w in g) and all(name.split()[-1].startswith(w.split()[-1][0]) for w in g):
						g.append(name)
						break
				else:
					if all(fuzz.token_sort_ratio(name, w) > 72 for w in g) and all(name.startswith(w[0]) for w in g):
						g.append(name)
						break
			else:
				grs.append([name, ])
		#grs=[{x[0]:list(set(x))} for x in grs] #setting to avoid many identical forms
		#grs=[{x[0]:[i for i, t in enumerate(texto) for entidad in list(set(x)) if t==entidad ]} for x in grs]
		
		#grs=[{x[0]:[" ".join(texto[y[1][0]-7:y[1][1]+7]) for y in formas_index for entidad in list(set(x)) if entidad==y[0] ]} for x in grs]
		grs=[{x[0]:[[" ".join(texto[y[1][0]-7:y[1][0]]), " ".join(texto[y[1][0]:y[1][1]]), " ".join(texto[y[1][1]:y[1][1]+7])] for y in forms_index for entidad in list(set(x)) if entidad==y[0] ]} for x in grs]

		
		entity_forms_groups[k]=grs
	return entity_forms_groups


#-----------inicio de la aplicación-----------------------

@app.route('/')
def home():
	results=None
	return render_template('index.html')
	

@app.route('/process',methods=['POST'])
def process():

	if request.method == 'POST':
		multiple_files=False
		texto = " ".join(request.form.get('text').split("\n"))
		#choice = request.form['taskoption']
		if texto:
			text=grafias(texto)
			choice = request.form.getlist('taskoption')[0]
			#as_dict=as_dict[0]
		else:
			#choice = request.form['taskoption']
			if 'file' in request.files:
				files=request.files.getlist('file')
				text=[]
				#index_arch_multiples=[0]
				for file in files:
					multiple_files=True
					text_a=file.read()
					text_a=str(text_a, encoding = 'utf-8')
					text_a=" ".join(text_a.split("\n"))
					#x=x.encode("latin-1").decode("latin-1")
					#text_a=grafias(text_a)
					text_a=" . ".join(" , ".join(text_a.split(",")).split("."))
					text.append(grafias(text_a))
					#index_arch_multiples.append(len(text_a.split()))
					

				if len(files)>1:
					text=" $ ".join(text)
					
				else:
					text=text[0]

		choice = request.form.getlist('taskoption')[0]
				
	
		tagger=treetaggerwrapper.TreeTagger(TAGLANG='la', TAGDIR=os.path.abspath(os.getcwd())+"/treetagger")
		tags_cap=tagger.tag_text(text) 
		tags_cap=[x.split("\t")[0] for x in tags_cap]

		tags=tagger.tag_text(grafias(text).lower()) 
		tags=[x.split("\t") for x in tags]
		#tags=[[x[0], x[1], x[2].split()[0]] for x in tags]
		#'Ego', 'PRO', 'ego', 'moi,'
		tags=[[x[0], x[1], x[2].split("_")[0], x[2].split("_")[1]] if "_" in x[2] else [x[0], x[1], x[2].split("_")[0], "-"]  for x in tags]

		suffix_word=[x[0][-3::] for x in tags]#terminación de cada palabra

		cap_word=["UPPER" if x[0][0].isupper() else "LOWER" for x in tags]#capital o minuscula

		#['uniuersis', 'QLF', 'uniuersi', 'tous', 'LOWER', 'sis', 'O', 'O']
		acta=[[w] + [x[1]] + [x[2]] + [x[3]] + [y] + [z] for w, x, y, z in zip(tags_cap, tags, cap_word, suffix_word)]

		
		PERS_LOC_Sentence= Sentence(tags_cap)

		PERS_LOC_model.predict(PERS_LOC_Sentence)


		def models2conll(args=[PERS_LOC_Sentence, PERS_LOC_Sentence]):
			final_result=[]
			for x in args:
				try:
					n=str(x).split('Token-Labels: "')[1][:-2]; main_text=[]; index=[]; count_index=1
					for i, x in enumerate(n.split()):
						if x.startswith("<"): index.append([i-count_index, x[1:-1]]); count_index+=1
						else: main_text.append(x)

					conll=["O"]*len(main_text)
					for x in index: conll[x[0]]=x[1]
				except:
					main_text=[token.text for token in x]
					conll=["O"]*len(main_text)
				
				final_result.append(conll)

			final_result[0]=[x if x!="L-PERS" else "I-PERS" for x in final_result[0]]
			final_result[1]=[x if (x!="B-PERS" and x!="I-PERS") else "O" for x in final_result[1]]
			final_result_loc=[]
			for i, x in enumerate(final_result[1]):
				if x=="L-PERS" and final_result[1][i-1]=="O":
					final_result_loc.append("B-LOC")
				elif x=="L-PERS" and final_result[1][i-1]!="L-PERS":
					final_result_loc.append("I-LOC")
				else:
					final_result_loc.append(x)
			final_result[1]=final_result_loc

			final_result=list(zip(*final_result))

			final_result=[acta[i]+list(x) for i,x in enumerate(final_result)]
			
			return final_result



		entities=models2conll()

        #tags=[[tags_cap[i], x[1], x[2].split("_", 1)[0], entities[i], kk] for i,x in enumerate(tags)]

		n=[x[0] for x  in entities]#seleccionamos solo el texto


		#m=list(zip(extra, y_pred[0]))#m contiene una lista de tuplas con entidad y tipo.
		global BIO_CONLL
		BIO_CONLL=entities#columnas extra mas prediccion en ultimo lugar
		#m contiene en orden: ['Fretoi', 'NAM', 'Fretoi', 'Fretoi', 'I-PERS', 'B-LOC']
		
		global tei_4
		global json_BIO
		if choice == "persons":
			indices=indexation(classes[0], 6, m=BIO_CONLL)
			tei_4=inicio+" ".join(tei_build(indices, "tei"))+final
			tei_5=Markup('<p class="subrayado">'+" ".join(tei_build(indices, "span"))+"</p>")
			json_BIO=label_studio(indices, m=BIO_CONLL)
			dict_formas=centroid_forms(indices, n=n)
			results=[["*"+x[0], x[1], x[2], x[6]] if x[6]==classes[0] or x[6]==classes[1] else [x[0], x[1], x[2], "O"] for x in BIO_CONLL]
			num_of_results = len([x[0] for x in BIO_CONLL if x[6]=="B-PERS"])
		
		elif choice == "places":
			indices=indexation(classes_2[0], 7, m=BIO_CONLL)
			tei_4=inicio+" ".join(tei_build(indices, "tei"))+final
			tei_5=Markup('<p class="subrayado">'+" ".join(tei_build(indices, "span"))+"</p>")
			json_BIO=label_studio(indices, m=BIO_CONLL)
			dict_formas=centroid_forms(indices, n=n)
			results=[["*"+x[0], x[1], x[2], "O", x[7]] if x[7]==classes_2[0] or x[7]==classes_2[1] else [x[0], x[1], x[2],"O", "O"] for x in BIO_CONLL]
			num_of_results = len([x[0] for x in BIO_CONLL if x[7]=="B-LOC"])
			
			
		else:
			indices=indexation(classes[0], 6, m=BIO_CONLL)+indexation(classes_2[0], 7, m=BIO_CONLL)
			tei_4=inicio+" ".join(tei_build(indices, "tei"))+final
			tei_5=Markup('<p class="subrayado">'+" ".join(tei_build(indices, "span"))+"</p>")
			json_BIO=label_studio(indices, m=BIO_CONLL)
			dict_formas=centroid_forms(indices, n=n)
			results_pers=[["*"+x[0], x[1], x[2], x[6]] if x[6]==classes[0] or x[6]==classes[1] else [x[0], x[1], x[2], "O"] for x in BIO_CONLL]
			results_loc=[results_pers[i]+[x[7]] if x[7]==classes_2[0] or x[7]==classes_2[1] else results_pers[i]+["O"] for i,x in enumerate(BIO_CONLL)]
			results=results_loc
			num_of_results = len([x[0] for x in BIO_CONLL if x[7]=="B-LOC" or x[6]=="B-PERS"])
		
		#num_of_results = len([x[0] for x in m if x[1]=="B-PERS" ])
		

	json_BIO=json.dumps(json_BIO)

	#diseño de los json para label-studio según la talla de m cuando es mayor a 400 palabras
	#dict_json es un diccionario de completions donde la key de cada una es "swap_n"
	dict_json={}
	n=300
	nn=[x[0] for x in BIO_CONLL]
	if multiple_files:
		for index, item in enumerate(nn):
			count=0
			if item=="$":
				count+=1
				BIO_CONLL[index][0]="<<DOCUMENT_"+str(count)+">>"

		#index_b=[index for index, item in enumerate(nn) if item=="$"]+[len(nn)]#index de la division de cada documento
		#index_b=[-1]+index_b #lista de los indices
		#nueva_m=[m[index_b[i]+1:index_b[i+1]]for i in range(len(index_b)-1)]
	

	nueva_m=[BIO_CONLL[i:i+n] for i in range(0,len(BIO_CONLL),n)]#nueva_m es una lista de df's de maximo 350 palabras, es decir sub_m's
	
	for index,item in enumerate(nueva_m):#cada item es un df de 350 palabras, es decir un swap
		if choice == "person":
			dict_json["swap_"+str(index)]=label_studio(indexation(classes[0], 6, m=item), m=item, ID=index)
			#normalmente esto se pasa sobre el m entero, pero aqui es un sub-m, por eso m=item
		elif choice == "place":
			dict_json["swap_"+str(index)]=label_studio(indexation(classes[0], 7, m=item), m=item, ID=index)
		else:
			dict_json["swap_"+str(index)]=label_studio(indexation(classes[0], 6, m=item)+indexation(classes_2[0], 7, m=item), m=item, ID=index)

	#dict_json=jsonify(dict_json)


	import datetime
	basename = "mydocfile"
	suffix = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
	doc_file = "_".join(["DB/", basename, suffix, ".json"]) # e.g. 'mylogfile_120508_171442'
	with open(doc_file, 'w') as json_file:
		json.dump(dict_json, json_file)

	dict_json_2=dict_json
	dict_json_3=json.dumps(dict_json, indent=3)
	#dict_formas=json.dumps(dict_formas, indent=2)
	dict_json=json.dumps(dict_json)
	return render_template("index.html",results=results,num_of_results = num_of_results , tei_4=tei_4, tei_5= tei_5, json_BIO=json_BIO, m=BIO_CONLL, dict_json=dict_json, dict_json_2=dict_json_2, dict_json_3=dict_json_3, dict_formas=dict_formas)


#procesamientos extra
@app.route('/process_2',methods=['POST'])
def process_2():
	prueba=BIO_CONLL
	return render_template("index.html",prueba=prueba)
	

@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/download')
def download():
    # stream the response as the data is generated
    response = Response(tei_4, mimetype='application/xml')
    # add a filename
    response.headers.set("Content-Disposition", "attachment",filename="simulation.xml")
    return response

	

@app.route('/results',methods=['POST'])
def results():

    data = request.get_json(force=True)
    prediction = MODEL_PERS.predict([np.array(list(data.values()))])

    output = prediction[0]
    return jsonify(output)

if __name__ == "__main__":
    app.run(debug=True)
